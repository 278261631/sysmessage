package com.ie.sysmessage;

/**
 * Created by Administrator on 2017/10/3.
 */
public class DeviceStatus {
    /**
     * 设备主要颜色
     */
    private String deviceColor;
    /**
     * 消息颜色
     */
    private String messageColor;
    /**
     * 类型 目前是自定义任何类型
     */
    private String msgType;
    /**
     * 设备名称
     */
    private String deviceName;
    private String deviceID;
    /**
     * 状态
     */
    private String deviceStatus;
    private String telescopeStatus;
    private String cameraStatus;
    private boolean obsOffLine;

    public String getCameraCooler() {
        return cameraCooler;
    }

    public void setCameraCooler(String cameraCooler) {
        this.cameraCooler = cameraCooler;
    }

    private String cameraCooler;
    private String guideCameraStatus;
    private String guideFWHM;
    private String planName;
    private String planTarget;
    private String planProcess;
    private String planRepeate;
    private String planFilter;
    private String planCount;


    private String messageTime;
    private String cameraImage;
    private String fitsInJpegImage;
    private String ossPath;
    /**
     * 当前任务名
     */
    private String taskName;
    /**
     * 详细状态信息
     */
    private String span_utc_time_   ;
    private String span_local_time_;
    private String span_scope_status_;
    private String span_scope_ra_   ;
    private String span_scope_dec_  ;
    private String span_scope_az_   ;
    private String span_scope_alt_  ;
    private String span_ccd_status_  ;
    private String span_ccd_filter_  ;
    private String all_ccd_filters  ;
    private String span_ccd_bin_  ;
    private String s_sm_guideStat  ;
    private String consoleText  ;

    public String getCcd_fov() {
        return ccd_fov;
    }

    public void setCcd_fov(String ccd_fov) {
        this.ccd_fov = ccd_fov;
    }

    private String ccd_fov ;

    /**
     * 导星误差
     */
    private String span_sm_trkGraphX_    ;
    private String span_sm_trkGraphY_    ;


    public String getS_sm_guideStat() {
        return s_sm_guideStat;
    }

    public void setS_sm_guideStat(String s_sm_guideStat) {
        this.s_sm_guideStat = s_sm_guideStat;
    }

    public String getSpan_sm_trkGraphX_() {
        return span_sm_trkGraphX_;
    }

    public void setSpan_sm_trkGraphX_(String span_sm_trkGraphX_) {
        this.span_sm_trkGraphX_ = span_sm_trkGraphX_;
    }

    public String getSpan_sm_trkGraphY_() {
        return span_sm_trkGraphY_;
    }

    public void setSpan_sm_trkGraphY_(String span_sm_trkGraphY_) {
        this.span_sm_trkGraphY_ = span_sm_trkGraphY_;
    }

    public String getSpan_sm_trkGraphXY_() {
        return span_sm_trkGraphXY_;
    }

    public void setSpan_sm_trkGraphXY_(String span_sm_trkGraphXY_) {
        this.span_sm_trkGraphXY_ = span_sm_trkGraphXY_;
    }

    private String span_sm_trkGraphXY_   ;

    public String getConsoleText() {
        return consoleText;
    }

    public void setConsoleText(String consoleText) {
        this.consoleText = consoleText;
    }

    public DeviceStatus(){

    }

    public DeviceStatus( String msgType , String deviceName, String deviceID , String deviceStatus, String messageTime , String deviceColor , String messageColor){
        setMsgType(msgType);
        setDeviceName(deviceName);
        setDeviceID(deviceID);
        setDeviceStatus(deviceStatus);
        setMessageTime(messageTime);
        setDeviceColor(deviceColor);
        setMessageColor(messageColor);
    }

    public DeviceStatus( String msgType , String deviceName , String deviceID , String deviceStatus, String messageTime , String deviceColor , String messageColor,
                        String span_utc_time_   , String span_local_time_, String span_scope_status_, String span_scope_ra_   , String span_scope_dec_  ,
                        String span_scope_az_   , String span_scope_alt_  , String span_ccd_status_  , String span_ccd_filter_  , String span_ccd_bin_ ,String all_ccd_filters){

        setMsgType(msgType);
        setDeviceName(deviceName);
        setDeviceID(deviceID);
        setDeviceStatus(deviceStatus);
        setMessageTime(messageTime);
        setDeviceColor(deviceColor);
        setMessageColor(messageColor);

        setSpan_utc_time_(span_utc_time_);
        setSpan_local_time_(span_local_time_);
        setSpan_scope_status_(span_scope_status_);
        setSpan_scope_ra_(span_scope_ra_);
        setSpan_scope_dec_(span_scope_dec_);
        setSpan_scope_az_(span_scope_az_);
        setSpan_scope_alt_(span_scope_alt_);
        setSpan_ccd_status_(span_ccd_status_);
        setSpan_ccd_filter_(span_ccd_filter_);
        setSpan_ccd_bin_(span_ccd_bin_);
        setAll_ccd_filters(all_ccd_filters);
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getDeviceColor() {
        return deviceColor;
    }

    public void setDeviceColor(String deviceColor) {
        this.deviceColor = deviceColor;
    }

    public String getMessageColor() {
        return messageColor;
    }

    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    public String getCameraImage() {
        return cameraImage;
    }

    public void setCameraImage(String cameraImage) {
        this.cameraImage = cameraImage;
    }

    public String getFitsInJpegImage() {
        return fitsInJpegImage;
    }

    public void setFitsInJpegImage(String fitsInJpegImage) {
        this.fitsInJpegImage = fitsInJpegImage;
    }

    public String getSpan_utc_time_() {
        return span_utc_time_;
    }

    public void setSpan_utc_time_(String span_utc_time_) {
        this.span_utc_time_ = span_utc_time_;
    }

    public String getSpan_local_time_() {
        return span_local_time_;
    }

    public void setSpan_local_time_(String span_local_time_) {
        this.span_local_time_ = span_local_time_;
    }

    public String getSpan_scope_status_() {
        return span_scope_status_;
    }

    public void setSpan_scope_status_(String span_scope_status_) {
        this.span_scope_status_ = span_scope_status_;
    }

    public String getSpan_scope_ra_() {
        return span_scope_ra_;
    }

    public void setSpan_scope_ra_(String span_scope_ra_) {
        this.span_scope_ra_ = span_scope_ra_;
    }

    public String getSpan_scope_dec_() {
        return span_scope_dec_;
    }

    public void setSpan_scope_dec_(String span_scope_dec_) {
        this.span_scope_dec_ = span_scope_dec_;
    }

    public String getSpan_scope_az_() {
        return span_scope_az_;
    }

    public void setSpan_scope_az_(String span_scope_az_) {
        this.span_scope_az_ = span_scope_az_;
    }

    public String getSpan_scope_alt_() {
        return span_scope_alt_;
    }

    public void setSpan_scope_alt_(String span_scope_alt_) {
        this.span_scope_alt_ = span_scope_alt_;
    }

    public String getSpan_ccd_status_() {
        return span_ccd_status_;
    }

    public void setSpan_ccd_status_(String span_ccd_status_) {
        this.span_ccd_status_ = span_ccd_status_;
    }

    public String getSpan_ccd_filter_() {
        return span_ccd_filter_;
    }

    public void setSpan_ccd_filter_(String span_ccd_filter_) {
        this.span_ccd_filter_ = span_ccd_filter_;
    }

    public String getSpan_ccd_bin_() {
        return span_ccd_bin_;
    }

    public void setSpan_ccd_bin_(String span_ccd_bin_) {
        this.span_ccd_bin_ = span_ccd_bin_;
    }

    public String getOssPath() {
        return ossPath;
    }

    public void setOssPath(String ossPath) {
        this.ossPath = ossPath;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getAll_ccd_filters() {
        return all_ccd_filters;
    }

    public void setAll_ccd_filters(String all_ccd_filters) {
        this.all_ccd_filters = all_ccd_filters;
    }

    public String getTelescopeStatus() {
        return telescopeStatus;
    }

    public boolean isObsOffLine() {
        return obsOffLine;
    }

    public void setObsOffLine(boolean obsOffLine) {
        this.obsOffLine = obsOffLine;
    }

    public void setTelescopeStatus(String telescopeStatus) {
        this.telescopeStatus = telescopeStatus;
    }

    public String getCameraStatus() {
        return cameraStatus;
    }

    public void setCameraStatus(String cameraStatus) {
        this.cameraStatus = cameraStatus;
    }

    public String getGuideCameraStatus() {
        return guideCameraStatus;
    }

    public void setGuideCameraStatus(String guideCameraStatus) {
        this.guideCameraStatus = guideCameraStatus;
    }

    public String getGuideFWHM() {
        return guideFWHM;
    }

    public void setGuideFWHM(String guideFWHM) {
        this.guideFWHM = guideFWHM;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanTarget() {
        return planTarget;
    }

    public void setPlanTarget(String planTarget) {
        this.planTarget = planTarget;
    }

    public String getPlanProcess() {
        return planProcess;
    }

    public void setPlanProcess(String planProcess) {
        this.planProcess = planProcess;
    }

    public String getPlanRepeate() {
        return planRepeate;
    }

    public void setPlanRepeate(String planRepeate) {
        this.planRepeate = planRepeate;
    }

    public String getPlanFilter() {
        return planFilter;
    }

    public void setPlanFilter(String planFilter) {
        this.planFilter = planFilter;
    }

    public String getPlanCount() {
        return planCount;
    }

    public void setPlanCount(String planCount) {
        this.planCount = planCount;
    }
}
