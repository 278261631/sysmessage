package com.ie.sysmessage;

import java.util.List;

public class TaskTarget {


	/**
	 *
	 */
	private String targetName;
	/**
	 * Ra坐标
	 */
	private double task_Ra_deg;
	/**
	 * Dec坐标
	 */
	private double task_Dec_deg;


	/**
	 * 单目标重复次数
	 */
	private int task_repeat;

	/**
	 * 关键拍摄参数
	 * 每一组滤镜对应一个时长 和 重复次数
	 * 例:Red,4,200,1代表 红色滤镜 bin=4,拍摄200秒,一共拍摄1次
	 */
	private List<String> filterBinningIntervalCount;

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public double getTask_Ra_deg() {
		return task_Ra_deg;
	}

	public void setTask_Ra_deg(double task_Ra_deg) {
		this.task_Ra_deg = task_Ra_deg;
	}

	public double getTask_Dec_deg() {
		return task_Dec_deg;
	}

	public void setTask_Dec_deg(double task_Dec_deg) {
		this.task_Dec_deg = task_Dec_deg;
	}

	public int getTask_repeat() {
		return task_repeat;
	}

	public void setTask_repeat(int task_repeat) {
		this.task_repeat = task_repeat;
	}

	public List<String> getFilterBinningIntervalCount() {
		return filterBinningIntervalCount;
	}

	public void setFilterBinningIntervalCount(List<String> filterBinningIntervalCount) {
		this.filterBinningIntervalCount = filterBinningIntervalCount;
	}
}