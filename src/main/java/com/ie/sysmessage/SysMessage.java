package com.ie.sysmessage;

public class SysMessage {

	private String sys_from_name;
	private String sys_to_name;
	private String encrypt_pub_key;
	private String msg_type;
	private TaskData task_data;
	private DeviceStatus deviceStatus;
	
	public SysMessage(){
		
	}
	
	public String getSys_from_name() {
		return sys_from_name;
	}
	public void setSys_from_name(String sys_from_name) {
		this.sys_from_name = sys_from_name;
	}
	public String getSys_to_name() {
		return sys_to_name;
	}
	public void setSys_to_name(String sys_to_name) {
		this.sys_to_name = sys_to_name;
	}
	public String getEncrypt_pub_key() {
		return encrypt_pub_key;
	}
	public void setEncrypt_pub_key(String encrypt_pub_key) {
		this.encrypt_pub_key = encrypt_pub_key;
	}
	public String getMsg_type() {
		return msg_type;
	}
	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}
	public TaskData getTask_data() {
		return task_data;
	}
	public void setTask_data(TaskData task_data) {
		this.task_data = task_data;
	}
	public DeviceStatus getDeviceStatus() {	return deviceStatus;}
	public void setDeviceStatus(DeviceStatus deviceStatus) {this.deviceStatus = deviceStatus;}
}