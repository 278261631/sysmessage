package com.ie.sysmessage;

/**
 * Created by Administrator on 2017/10/3.
 */
public class MoonSunStatus {
    private String sun_rise ;
    private Double moon_illumination;
    private Double moon_ra_deg;
    private Double moon_dec_deg;
    private Double moon_alt_deg;
    private Double sun_alt_deg;

    public String getSun_rise() {
        return sun_rise;
    }

    public void setSun_rise(String sun_rise) {
        this.sun_rise = sun_rise;
    }

    public Double getMoon_illumination() {
        return moon_illumination;
    }

    public void setMoon_illumination(Double moon_illumination) {
        this.moon_illumination = moon_illumination;
    }

    public Double getMoon_ra_deg() {
        return moon_ra_deg;
    }

    public void setMoon_ra_deg(Double moon_ra_deg) {
        this.moon_ra_deg = moon_ra_deg;
    }

    public Double getMoon_dec_deg() {
        return moon_dec_deg;
    }

    public void setMoon_dec_deg(Double moon_dec_deg) {
        this.moon_dec_deg = moon_dec_deg;
    }

    public Double getMoon_alt_deg() {
        return moon_alt_deg;
    }

    public void setMoon_alt_deg(Double moon_alt_deg) {
        this.moon_alt_deg = moon_alt_deg;
    }

    public Double getSun_alt_deg() {
        return sun_alt_deg;
    }

    public void setSun_alt_deg(Double sun_alt_deg) {
        this.sun_alt_deg = sun_alt_deg;
    }
}
