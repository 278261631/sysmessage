package com.ie.sysmessage;

import java.util.List;
import java.util.Map;

public class TaskData {


	/**
	 * 任务名 每次发送应当唯一,用于跟踪任务进程
	 */
	private String taskName;
	/**
	 * Ra坐标
	 */
	@Deprecated
	private double task_Ra_deg;
	/**
	 * Dec坐标
	 */
	@Deprecated
	private double task_Dec_deg;

	private List<TaskTarget> task_targets;

	/**
	 * 任务等级 默认100 ,会被高级任务强制替换
	 */
	private String  task_level;
	/**
	 * 废弃
	 */
	@Deprecated
	private String task_start_time;
	/**
	 * 废弃
	 */
	@Deprecated
	private String task_end_time;

//	/**
//	 * 单目标重复次数
//	 */
//	private int task_repeat;

	/**
	 * 整个plan 重复次数
	 */
	private int task_sets;

	/**
	 * 需要执行的设备,如果需要多台设备执行,需要在设备端配置多个监听
	 */
	private String target_eqp;
//	private double interval_second;
	// filter and interval
	/**
	 * 关键拍摄参数
	 * 每一组滤镜对应一个时长 和 重复次数
	 * 例:Red,4,200,1代表 红色滤镜 bin=4,拍摄200秒,一共拍摄1次
	 */
	@Deprecated
	private List<String> filterBinningIntervalCount;
	/**
	 * 未启用
	 */
	private String task_status;

	public String getTask_type() {
		return task_type;
	}

	public void setTask_type(String task_type) {
		this.task_type = task_type;
	}

	/**
	 * GRAVITATIONALWAVE
	 * GCN
	 * NORMAL
	 */
	private String task_type;

	public String getTask_plan_text() {
		return task_plan_text;
	}

	public void setTask_plan_text(String task_plan_text) {
		this.task_plan_text = task_plan_text;
	}

	private String task_plan_text;

	/**
	 * stop,park?connect?telescope?ccdian
	 */
	private String task_command;

	public TaskData(){
		
	}
	@Deprecated
	public TaskData(String name,double ra_deg,double dec_deg,String level) {
		this.taskName=name;
		this.task_Ra_deg=ra_deg;
		this.task_Dec_deg=dec_deg;
		this.task_level=level;
	}

	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	@Deprecated
	public double getTask_Ra_deg() {
		return task_Ra_deg;
	}
	@Deprecated
	public void setTask_Ra_deg(double task_Ra_deg) {
		this.task_Ra_deg = task_Ra_deg;
	}
	@Deprecated
	public double getTask_Dec_deg() {
		return task_Dec_deg;
	}
	@Deprecated
	public void setTask_Dec_deg(double task_Dec_deg) {
		this.task_Dec_deg = task_Dec_deg;
	}

	public String getTask_level() {
		return task_level;
	}
	public void setTask_level(String task_level) {
		this.task_level = task_level;
	}
	public String getTask_start_time() {
		return task_start_time;
	}
	public void setTask_start_time(String task_start_time) {
		this.task_start_time = task_start_time;
	}
	public String getTask_end_time() {
		return task_end_time;
	}
	public void setTask_end_time(String task_end_time) {

		this.task_end_time = task_end_time;
	}

	public String getTarget_eqp() {
		return target_eqp;
	}

	public void setTarget_eqp(String target_eqp) {
		this.target_eqp = target_eqp;
	}

//	public double getInterval_second() {
//		return interval_second;
//	}

//	public void setInterval_second(double interval_second) {
//		this.interval_second = interval_second;
//	}

	public String getTask_status() {
		return task_status;
	}

	public void setTask_status(String task_status) {
		this.task_status = task_status;
	}
	@Deprecated
    public List<String> getFilterBinningIntervalCount() {
        return filterBinningIntervalCount;
    }
	@Deprecated
    public void setFilterBinningIntervalCount(List<String> filterBinningIntervalCount) {
        this.filterBinningIntervalCount = filterBinningIntervalCount;
    }

	public int getTask_sets() {
		return task_sets;
	}

	public void setTask_sets(int task_sets) {
		this.task_sets = task_sets;
	}

	public List<TaskTarget> getTask_targets() {
		return task_targets;
	}

	public void setTask_targets(List<TaskTarget> task_targets) {
		this.task_targets = task_targets;
	}

	public String getTask_command() {
		return task_command;
	}

	public void setTask_command(String task_command) {
		this.task_command = task_command;
	}
}